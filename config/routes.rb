Rails.application.routes.draw do

  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  authenticated :user do
    root to: 'welcome#index'
    resources :payments,only:[:pay_tickets,:checkout] do
      collection do
        post'pay_tickets/:id/tickets/:tickets' => :pay_tickets,as: :pay_tickets
        get 'checkout' => :checkout,as: :checkout
      end
    end  
    resources :concerts do
      collection do
        get 'show_tickets/:id' => :show_tickets,as: :show_tickets
        get 'create_order_pay/:id'=> :create_order_pay,as: :create_order_pay
      end
    end
  end

  unauthenticated :user do
    root to:'welcome#unregistered'
    resources :concerts,only:[:show]
  end
end
