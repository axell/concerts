class WelcomeController < ApplicationController
  before_action :set_concerts
  def index
  end

  def unregistered
  end

  def set_concerts
    @concerts = Concert.all.order(name: :desc)
  end
end
