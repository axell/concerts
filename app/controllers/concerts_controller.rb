class ConcertsController < ApplicationController
  before_action :set_concert, only: [:show, :edit, :update, :destroy,
                                      :show_tickets, :create_order_pay]
  before_action :authenticate_user!
  before_action :authenticate_admin!, only: [:create, :new, :update,
                                              :destroy, :index]
  def show_tickets 
  end

  def create_order_pay
    @number_of_tickets = params[:number_of_tickets]
  end

  def index
    @concerts = Concert.all
  end

  def show
  end

  def new
    @concert = Concert.new
  end

  def edit
  end

  def create
    @concert = current_user.concerts.new(concert_params)

    respond_to do |format|
      if @concert.save
        format.html { redirect_to concerts_path, notice: 'Concierto creado.'}
      else
        format.html { render :new }
      end
    end
  end

  def update
    respond_to do |format|
      if @concert.update(concert_params)
        format.html { redirect_to concerts_path, notice: 'Concierto actualizado.'}
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    @concert.destroy
    respond_to do |format|
      format.html { redirect_to concerts_url, notice: 'Concierto eliminado.'}
    end
  end

  private

    def set_concert
      @concert = Concert.friendly.find(params[:id])
      @tickets = params[:tickets]
    end

    def authenticate_admin!
      if !current_user.is_admin?
        redirect_to root_path, 
          alert: 'no puedes realizar esta acción',status: :unauthorized 
      end
    end

    def concert_params
      params.require(:concert).permit(:name, :description, :places, :price)
    end
end