class PaymentsController < ApplicationController

  before_action :set_concert, only:[:pay_tickets]
  
  include PayPal::SDK::REST

  def pay_tickets
    payment = Payment.new({
      intent: 'sale',
      payer:{
        payment_method: 'paypal'
      },
      transactions:[
        {
          item_list:{
            items:[{name: 'boletos', sku: :item , price:(@concert.price / 100) , currency:'USD', quantity: @tickets  }]
          },
          amount:{
            total:(@concert.get_total(@tickets) / 100),
            currency:'USD'
          },
          description: 'compra de tus boletos en nuestra plataforma'
        }
      ],
      redirect_urls:{
        return_url: 'https://fast-savannah-49643.herokuapp.com/payments/checkout',
        cancel_url:  'https://fast-savannah-49643.herokuapp.com/'
      }
    })

    if @tickets.to_i > @concert.places
      redirect_to concerts_path,alert:'no hay la cantidad de boletos solicitados'
    else
      if payment.create
        @my_payment = MyPayment.create!(paypal_id: payment.id, ip: request.remote_ip, user_id: current_user.id,
          concert_id: @concert.id, quantity: @tickets)
        redirect_to payment.links.find{|v| v.method == 'REDIRECT'}.href
      else
        raise payment.error.to_yaml 
      end
    end
  end


  def checkout
    
    @my_payment = MyPayment.find_by(paypal_id: params[:paymentId]) 
    if @my_payment.nil? 
      redirect_to concerts_path
    else
      concert = Concert.find(@my_payment.concert_id)
      payment = Payment.find(@my_payment.paypal_id)
      if payment.execute(payer_id: params[:PayerID])
        concert.update_concert_tickets(@my_payment.quantity)
        @my_payment.pay!

        redirect_to concert_path(concert.id),notice: 'Ha comprado sus boletos exitosamente'
      else
        redirect_to concert_path(concert.id),alert: 'Hubo problemas al procesar su pago'
      end
    end
  end

  private
    def set_concert
      @concert = Concert.friendly.find(params[:id])
      @tickets = params[:tickets]
    end  
end