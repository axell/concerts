# == Schema Information
#
# Table name: my_payments
#
#  id         :bigint           not null, primary key
#  email      :string
#  fee        :decimal(6, 2)
#  ip         :string
#  quantity   :integer
#  status     :string
#  total      :decimal(10, 2)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  concert_id :bigint
#  paypal_id  :string
#  user_id    :bigint
#
# Indexes
#
#  index_my_payments_on_concert_id  (concert_id)
#  index_my_payments_on_user_id     (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (concert_id => concerts.id)
#  fk_rails_...  (user_id => users.id)
#
class MyPayment < ApplicationRecord

  include AASM

  aasm column: 'status' do

    state :created, initial: true
    state :payed
    state :failed

    event :pay do 
      transitions from: :created, to: :payed
    end 
    
  end

end
