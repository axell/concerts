# == Schema Information
#
# Table name: concerts
#
#  id          :bigint           not null, primary key
#  description :text
#  name        :string
#  places      :integer
#  price       :decimal(10, 2)
#  slug        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  user_id     :bigint
#
# Indexes
#
#  index_concerts_on_slug     (slug) UNIQUE
#  index_concerts_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
class Concert < ApplicationRecord
  
  belongs_to :user
  
  extend FriendlyId
  friendly_id :name, use: :slugged
  
  validates :name, :description, :places, :price, presence: true

  # method for get the total prince in cards
  def get_total(number_of_tickers)
  	self.price * number_of_tickers.to_i
  end
  # method for update the places in the concert
  def update_concert_tickets(tickets)
  	new_places = places - tickets
  	self.update(places: new_places)
  end
  #method for check if places are available in a concert
  def tickets_availables?
    self.places > 0
  end

end
