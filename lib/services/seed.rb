module Services
  class Seed
    
    def self.users
      User.create( email:"admin@gmail.com", password:"123456", password_confirmation:"123456" ,role:"admin")
      
    end

    def self.concerts

      Concert.create(name:"Lorem Ipsum",description:"Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                                        hen an unknown printer took a galley of type and scrambled it to make a type.",price:250.00,places:200,user_id:1)
    end
  end
end