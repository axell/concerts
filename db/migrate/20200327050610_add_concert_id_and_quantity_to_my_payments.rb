class AddConcertIdAndQuantityToMyPayments < ActiveRecord::Migration[5.2]
  def change
    add_reference :my_payments, :concert, foreign_key: true
    add_column :my_payments, :quantity, :integer
  end
end
