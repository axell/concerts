class AddSlugToConcerts < ActiveRecord::Migration[5.2]
  def change
    add_column :concerts, :slug, :string
    add_index :concerts, :slug, unique: true
  end
end
