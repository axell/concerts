class CreateConcerts < ActiveRecord::Migration[5.2]
  def change
    create_table :concerts do |t|
      t.string :name
      t.text :description
      t.integer :places
      t.decimal :price, precision:10, scale:2
      t.references :user, foreign_key: true
      
      t.timestamps
    end
  end
end
